using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Tests
{
    public class ConvertPagesToImageTest : TestBase
    {
        [Test]
        public async Task ConvertPagesAsJpgTest()
        {
            var param = new List<KeyValuePair<string, string>>();
            param.Add(new KeyValuePair<string, string>("sourcePath", "4pages.pdf"));

            UploadFile("4pages.pdf", @"..\..\..\..\4pages.pdf");

            var res = await PostRequest<string>("api/convertpages/jpg", param);
            Assert.Pass();
        }

        [Test]
        public async Task ConvertPagesAsPngTest()
        {
            var param = new List<KeyValuePair<string, string>>();
            param.Add(new KeyValuePair<string, string>("sourcePath", "4pages.pdf"));

            UploadFile("4pages.pdf", @"..\..\..\..\4pages.pdf");

            var res = await PostRequest<string>("api/convertpages/png", param);
            Assert.Pass();
        }

    }
}