using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using NUnit.Framework;
using WebApplicationAPI.DTO;

namespace Tests
{
    public class InvoiceTest : TestBase
    {
        [Test]
        public async Task CheckInvoiceAsPdfTest()
        {
            Invoice param = GetInvoice();

            List<KeyValuePair<string, string>> uriParam = new List<KeyValuePair<string, string>>();
            uriParam.Add(new KeyValuePair<string, string>("outputPath", "resInvoice.pdf"));

            var res = await PostRequest<string>("api/invoice/pdf", uriParam, param);
            Assert.Pass();
        }

        [Test]
        public async Task CheckInvoiceAsImageTest()
        {
            Invoice param = GetInvoice();

            List<KeyValuePair<string, string>> uriParam = new List<KeyValuePair<string, string>>();
            uriParam.Add(new KeyValuePair<string, string>("outputPath", "resInvoice.jpg"));

            var res = await PostRequest<string>("api/invoice/image", uriParam, param);
            Assert.Pass();
        }

        private Invoice GetInvoice()
        {
            var param = new Invoice();
            param.CustomerCity = "Kyiv";
            param.CustomerName = "Microsoft";
            param.CustomerNumber = "1234567";
            param.CustomerStreet = "Maydan nesaleznosti, 1";
            param.CustomerVATNumber = "987654321";
            param.HeaderCompanyInfo = new List<string>();
            param.HeaderCompanyInfo.Add("COMPANY COMPANY COMPANY COMPANY COMPANY COMPANY COMPANY");
            param.HeaderCompanyInfo.Add("PHONE FAX PHONE FAX PHONE FAX PHONE");
            param.HeaderCompanyInfo.Add("VAT VAT VAT VAT VAT VAT VAT VAT");
            param.HeaderCompanyInfo.Add("BANK BANK BANK BANK BANK BANK BANK BANK");
            param.InvoiceID = "4";
            param.SomeSentence = "SOME SENTENCE SOME SENTENCE SOME SENTENCE SOME SENTENCE SOME SENTENCE SOME SENTENCE:";
            param.Efforts = new List<Effort>();
            param.Efforts.Add(new Effort()
            {
                Amount = 50,
                Description = "Effort 1",
                VAT_Percentage = 20
            });
            param.Efforts.Add(new Effort()
            {
                Amount = 60,
                Description = "Effort 2",
                VAT_Percentage = 20
            });
            param.Efforts.Add(new Effort()
            {
                Amount = 70,
                Description = "Effort 3",
                VAT_Percentage = 20
            });
            return param;
        }

    }

}