using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Tests
{
    public class FetchHtmlTest : TestBase
    {
        [Test]
        public async Task MergePagesAsPdfTest()
        {
            List<KeyValuePair<string, string>> param = new List<KeyValuePair<string, string>>();
            param.Add(new KeyValuePair<string, string>("outputPath", "resPdf.pdf"));
            param.Add(new KeyValuePair<string, string>("urls", "https://aspose.com/"));
            param.Add(new KeyValuePair<string, string>("urls", "https://aspose.com/"));
            param.Add(new KeyValuePair<string, string>("urls", "https://aspose.com/"));
            param.Add(new KeyValuePair<string, string>("urls", "https://aspose.com/"));
            param.Add(new KeyValuePair<string, string>("urls", "https://aspose.com/"));
            param.Add(new KeyValuePair<string, string>("urls", "https://aspose.com/"));
            param.Add(new KeyValuePair<string, string>("urls", "https://aspose.com/"));
            param.Add(new KeyValuePair<string, string>("urls", "https://aspose.com/"));
            var res = await PostRequest<string>("api/webpagereader/pdf", param);
            Assert.Pass();
        }

        [Test]
        public async Task MergePagesAsImageTest()
        {
            List<KeyValuePair<string, string>> param = new List<KeyValuePair<string, string>>();
            param.Add(new KeyValuePair<string, string>("outputPath", "resImages.pdf"));
            param.Add(new KeyValuePair<string, string>("urls", "https://google.com/"));
            param.Add(new KeyValuePair<string, string>("urls", "https://aspose.com/"));
            var res = await PostRequest<string>("api/webpagereader/image", param);
            Assert.Pass();
        }

        //[Test]
        //public async Task Test2()
        //{
        //    var res = await GetRequest<string>("api/values");
        //    Assert.IsTrue(res.Contains("value1"));
        //}

    }

}