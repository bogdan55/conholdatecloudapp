using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Tests
{
    public class Tests : TestBase
    {
        [Test]
        public async Task Test1()
        {
            var res = await GetRequest<string>("api/values");
            Assert.IsTrue(res.Contains("value1"));
        }

        [Test]
        public async Task Test2()
        {
            var res = await GetRequest<string>("api/values");
            Assert.IsTrue(res.Contains("value1"));
        }

    }

}