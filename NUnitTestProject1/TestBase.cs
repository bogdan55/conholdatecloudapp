﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Aspose.Pdf.Cloud.Sdk.Api;
using Aspose.Pdf.Cloud.Sdk.Client;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Tests
{
    public class TestBase
    {
        //private readonly string _url = "http://localhost:61575/";
        private readonly string _url = "http://conholdate-cloud-app.189329.gw.k8s.saltov.dynabic.com/";
        private readonly string cloudBasePath = "https://api-qa.aspose.cloud/";

        private static string _accessToken = "";
        private string client_id = "ccba620a-1f42-414b-ba7c-c1d18260c361";
        private string client_secret = "2c76944d323561d3720de0fa7d94412e";
        private static PdfApi pdfApi;

        [SetUp]
        public async Task Setup()
        {
            if (string.IsNullOrEmpty(_accessToken))
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(cloudBasePath);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                var parameters = new Dictionary<string, string>
                {
                    {"grant_type", "client_credentials"},
                    {"client_id", client_id},
                    {"client_secret", client_secret}
                };
                var encodedContent = new FormUrlEncodedContent(parameters);
                var responseString = await (client.PostAsync("connect/token", encodedContent)).Result.Content
                    .ReadAsStringAsync();
                var result =
                    (GetAccessTokenResult)JsonConvert.DeserializeObject(responseString, typeof(GetAccessTokenResult));
                _accessToken = result.AccessToken;
                Configuration conf = new Configuration(null, null, cloudBasePath);
                pdfApi = new PdfApi(conf);
                pdfApi.ApiClient.AccessToken = _accessToken;
            }
        }

        internal void UploadFile(string fileName, string localFilePath)
        {
            using (var stream = new FileStream(localFilePath, FileMode.Open))
            {
                pdfApi.UploadFile(fileName, stream);
            }
        }

        internal async Task<T> GetRequest<T>(string url)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(_url);
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _accessToken);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                if (typeof(T) == typeof(string))
                {
                    return (T)(object)res;
                }
                else
                {
                    return JsonConvert.DeserializeObject<T>(res);
                }
            }
            else
            {
                throw new HttpRequestException();
            }
        }

        internal async Task<T> PostRequest<T>(string url, List<KeyValuePair<string, string>> uriParam)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(_url);
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _accessToken);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var uriParamString = "";
            foreach (KeyValuePair<string, string> keyValuePair in uriParam)
            {
                var separator = uriParamString.Contains("?") ? "&" : "?";
                uriParamString += $"{separator}{keyValuePair.Key}={keyValuePair.Value}";
            }
            url += uriParamString;
            client.Timeout = new TimeSpan(0, 0, 5, 0);
            var response = await client.PostAsync(url, null);
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                if (typeof(T) == typeof(string))
                {
                    return (T)(object)res;
                }
                else
                {
                    return JsonConvert.DeserializeObject<T>(res);
                }
            }
            else
            {
                throw new HttpRequestException();
            }
        }

        internal async Task<T> PostRequest<T>(string url, List<KeyValuePair<string, string>> uriParam, object bodyParam)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(_url);
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _accessToken);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            var uriParamString = "";
            foreach (KeyValuePair<string, string> keyValuePair in uriParam)
            {
                var separator = uriParamString.Contains("?") ? "&" : "?";
                uriParamString += $"{separator}{keyValuePair.Key}={keyValuePair.Value}";
            }
            url += uriParamString;
            client.Timeout = new TimeSpan(0, 0, 5, 0);
            var response = await client.PostAsync(url, new StringContent(JsonConvert.SerializeObject(bodyParam), Encoding.UTF8, "application/json"));
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                if (typeof(T) == typeof(string))
                {
                    return (T)(object)res;
                }
                else
                {
                    return JsonConvert.DeserializeObject<T>(res);
                }
            }
            else
            {
                throw new HttpRequestException();
            }
        }
        /// <summary>
        ///     Access token result
        /// </summary>
        private class GetAccessTokenResult
        {
            [JsonProperty(PropertyName = "access_token")]
            public string AccessToken { get; set; }
        }


    }
}