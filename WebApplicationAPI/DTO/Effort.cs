﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationAPI.DTO
{
    public class Effort
    {
        public float VAT_Percentage;
        public string Description { get; set; }
        public float Amount { get; set; }
    }
}
