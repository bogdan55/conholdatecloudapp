﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationAPI.DTO
{
    public class Invoice
    {
        public string InvoiceID;
        public string CustomerVATNumber;
        public List<string> HeaderCompanyInfo { get; set; }
        public string CustomerName { get; set; }
        public string CustomerStreet { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerCity { get; set; }
        public string SomeSentence { get; set; }
        public List<Effort> Efforts { get; set; }
    }
}
