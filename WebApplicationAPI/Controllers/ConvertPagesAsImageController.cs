﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Aspose.Pdf.Cloud.Sdk.Api;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplicationAPI.Business;

namespace WebApplicationAPI.Controllers
{
    [Route("api/convertpages")]
    [ApiController]
    public class ConvertPagesAsImageController : BaseController
    {
        public ConvertPagesAsImageController(IHostingEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor) : 
            base(hostingEnvironment, httpContextAccessor)
        {
        }

        [HttpPost]
        [Route("~/api/convertpages/png")]
        public ActionResult PostMergeAsImagePng([FromQuery]string sourcePath)
        {
            var fileName = "page";
            var manager = new PdfManager(sourcePath, htmlApi, pdfApi);
            int pageCount = manager.GetPageCount();
            for (int j = 1; j <= pageCount; j++)
            {
                manager.ExportPageAsImage(j, "png", fileName);
            }
            manager.MergeImagesToDoc(fileName, pageCount, "png");
            return new OkResult();
        }

        [HttpPost]
        [Route("~/api/convertpages/jpg")]
        public ActionResult PostMergeAsImageJpg([FromQuery]string sourcePath)
        {
            var fileName = "page";
            var manager = new PdfManager(sourcePath, htmlApi, pdfApi);
            int pageCount = manager.GetPageCount();
            for (int j = 1; j <= pageCount; j++)
            {
                manager.ExportPageAsImage(j, "jpg", fileName);
            }
            manager.MergeImagesToDoc(fileName, pageCount, "jpg");
            return new OkResult();
        }

    }
}
