﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Aspose.Pdf.Cloud.Sdk.Api;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplicationAPI.Business;
using WebApplicationAPI.DTO;

namespace WebApplicationAPI.Controllers
{
    [Route("api/invoice")]
    [ApiController]
    public class InvoiceController : BaseController
    {
        public InvoiceController(IHostingEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor) : 
            base(hostingEnvironment, httpContextAccessor)
        {
        }

        [HttpPost]
        [Route("~/api/invoice/pdf")]
        public ActionResult PostInvoiceAsPdf([FromQuery]string outputPath, [FromBody]Invoice invoice)
        {
            var manager = new InvoiceManager(htmlApi, pdfApi);
            manager.CreateDocument(outputPath);
            manager.FillDocument(outputPath, invoice);
            return new OkResult();
        }

        [HttpPost]
        [Route("~/api/invoice/image")]
        public ActionResult PostInvoiceAsImage([FromQuery]string outputPath, [FromBody]Invoice invoice)
        {
            var tempFile = "temp.pdf";
            var manager = new InvoiceManager(htmlApi, pdfApi);
            manager.CreateDocument(tempFile);
            manager.FillDocument(tempFile, invoice);
            manager.SaveAsImage(tempFile, outputPath);
            return new OkResult();
        }

    }
}
