﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Aspose.Pdf.Cloud.Sdk.Api;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplicationAPI.Business;

namespace WebApplicationAPI.Controllers
{
    [Route("api/webpagereader")]
    [ApiController]
    public class WebPageReaderController : BaseController
    {
        public WebPageReaderController(IHostingEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor) : 
            base(hostingEnvironment, httpContextAccessor)
        {
        }

        [HttpPost]
        [Route("~/api/webpagereader/pdf")]
        public ActionResult PostMergeAsPdf([FromQuery]string outputPath, [FromQuery]string[] urls)
        {
            var fileName = "page";
            var i = 0;
            var manager = new PdfManager(outputPath, htmlApi, pdfApi);
            foreach (var url in urls)
            {
                i++;
                manager.FetchPages(url, $"{fileName}{i}.html");
                manager.SaveAsPdf($"{fileName}{i}.html", $"{fileName}{i}.pdf");
            }
            manager.MergeDocs(fileName, i);
            return new OkResult();
        }

        [HttpPost]
        [Route("~/api/webpagereader/image")]
        public ActionResult PostMergeAsImage([FromQuery]string outputPath, [FromQuery]string[] urls)
        {
            var fileName = "page";
            var i = 0;
            var manager = new PdfManager(outputPath, htmlApi, pdfApi);
            foreach (var url in urls)
            {
                i++;
                manager.FetchPages(url, $"{fileName}{i}.html");
                manager.SaveAsImage($"{fileName}{i}.html", $"{fileName}{i}.jpg");
            }
            manager.MergeImagesToDoc(fileName, i, "jpg");
            return new OkResult();
        }

    }
}
