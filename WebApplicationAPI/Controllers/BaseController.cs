﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Aspose.Html.Cloud.Sdk.Api;
using Aspose.Pdf.Cloud.Sdk.Api;
using Aspose.Pdf.Cloud.Sdk.Client;

namespace WebApplicationAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        private string cloudBasePath = "https://api-qa.aspose.cloud/";
        private IHostingEnvironment _hostingEnvironment;
        private IHttpContextAccessor _httpContextAccessor;
        internal HtmlApi htmlApi;
        internal PdfApi pdfApi;

        public BaseController(IHostingEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            _hostingEnvironment = hostingEnvironment;
            _httpContextAccessor = httpContextAccessor;
            string token = _httpContextAccessor.HttpContext.Request.Headers["Authorization"];
            string bearerToken = string.Empty;
            Regex regexp = new Regex(@"Bearer\s+(?<token>\S+)", RegexOptions.IgnoreCase);
            if (!string.IsNullOrEmpty(token))
            {
                Match match = regexp.Match(token);
                if (match.Success)
                    bearerToken = match.Groups["token"].Value;
                htmlApi = new HtmlApi(bearerToken, $"{cloudBasePath}v3.0");
                Configuration conf = new Configuration(null,null, cloudBasePath);
                pdfApi = new PdfApi(conf);
                pdfApi.ApiClient.AccessToken = bearerToken;
            }
        }
    }
}
