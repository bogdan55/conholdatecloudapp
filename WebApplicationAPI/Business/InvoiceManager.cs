﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Aspose.Html.Cloud.Sdk.Api;
using Aspose.Html.Cloud.Sdk.Api.Interfaces;
using Aspose.Pdf.Cloud.Sdk.Api;
using Aspose.Pdf.Cloud.Sdk.Model;
using Microsoft.AspNetCore.Razor.Language;
using WebApplicationAPI.DTO;

namespace WebApplicationAPI.Business
{
    public class InvoiceManager
    {
        private readonly HtmlApi _htmlApi;
        private readonly PdfApi _pdfApi;

        public InvoiceManager(HtmlApi htmlApi, PdfApi pdfApi)
        {
            _htmlApi = htmlApi;
            _pdfApi = pdfApi;
        }

        public void CreateDocument(string fileName)
        {
            _pdfApi.PutCreateDocument(fileName);
        }

        public void FillDocument(string fileName, Invoice invoice)
        {
            //Create Company info
            CreateCompanyinfo(fileName, invoice);

            //--- Invoice Title ---
            CreateInvoiceTitle(fileName, invoice);

            //--- Invoice Info ---
            CreateInvoiceInfo(fileName, invoice);

            //--- Invoice Some Sentence ---
            CreateInvoiceSomeSentence(fileName, invoice);

            //--- Effort tabel ---
            CreateEffortTable(fileName, invoice);
        }

        private void CreateEffortTable(string fileName, Invoice invoice)
        {
            //Header
            var paragraphs1 = new List<TextRect>();
            paragraphs1.Add(new TextRect("Description", 1, HorizontalAlignment: HorizontalAlignment.Left, TextState: new TextState(10, "Helvetica-Bold")));
            var paragraphs2 = new List<TextRect>();
            paragraphs2.Add(new TextRect("Amount", 1, HorizontalAlignment: HorizontalAlignment.Left, TextState: new TextState(10, "Helvetica-Bold")));
            var paragraphs3 = new List<TextRect>();
            paragraphs3.Add(new TextRect("VAT", 1, HorizontalAlignment: HorizontalAlignment.Left, TextState: new TextState(10, "Helvetica-Bold")));
            var cells = new List<Cell>();
            cells.Add(new Cell()
            {
                Paragraphs = paragraphs1,
                ColSpan = 4,
                Border = new BorderInfo(Left: new GraphInfo(1), Top: new GraphInfo(1), Right: new GraphInfo(1), Bottom: new GraphInfo(1))
            });
            cells.Add(new Cell()
            {
                Paragraphs = paragraphs2,
                ColSpan = 1,
                Border = new BorderInfo(Left: new GraphInfo(1), Top: new GraphInfo(1), Right: new GraphInfo(1), Bottom: new GraphInfo(1))
            });
            cells.Add(new Cell()
            {
                Paragraphs = paragraphs3,
                ColSpan = 1,
                Border = new BorderInfo(Left: new GraphInfo(1), Top: new GraphInfo(1), Right: new GraphInfo(1), Bottom: new GraphInfo(1))
            });
            var rows = new List<Row>();
            rows.Add(new Row(Cells: cells)
            {
                DefaultCellTextState = new TextState(11, "Helvetica", FontStyle: FontStyles.Bold),
                BackgroundColor = new Color(0, 209, 209, 209)
            });

            //Rows
            foreach (Effort effort in invoice.Efforts)
            {
                var paragraphsRow1 = new List<TextRect>();
                paragraphsRow1.Add(new TextRect(effort.Description, 1, HorizontalAlignment: HorizontalAlignment.Left, TextState: new TextState(10, "Helvetica")));
                var paragraphsRow2 = new List<TextRect>();
                paragraphsRow2.Add(new TextRect(effort.Amount.ToString(), 1, HorizontalAlignment: HorizontalAlignment.Left, TextState: new TextState(10, "Helvetica")));
                var paragraphsRow3 = new List<TextRect>();
                paragraphsRow3.Add(new TextRect(effort.VAT_Percentage.ToString(), 1, HorizontalAlignment: HorizontalAlignment.Left, TextState: new TextState(10, "Helvetica")));
                var cellsRow = new List<Cell>();
                cellsRow.Add(new Cell()
                {
                    Paragraphs = paragraphsRow1,
                    ColSpan = 4,
                    Border = new BorderInfo(Left: new GraphInfo(1), Top: new GraphInfo(1), Right: new GraphInfo(1), Bottom: new GraphInfo(1))
                });
                cellsRow.Add(new Cell()
                {
                    Paragraphs = paragraphsRow2,
                    ColSpan = 1,
                    Border = new BorderInfo(Left: new GraphInfo(1), Top: new GraphInfo(1), Right: new GraphInfo(1), Bottom: new GraphInfo(1))
                });
                cellsRow.Add(new Cell()
                {
                    Paragraphs = paragraphsRow3,
                    ColSpan = 1,
                    Border = new BorderInfo(Left: new GraphInfo(1), Top: new GraphInfo(1), Right: new GraphInfo(1), Bottom: new GraphInfo(1))
                });
                rows.Add(new Row(Cells: cellsRow));
            }

            var tables = new List<Table>();
            tables.Add(new Table(Rows: rows)
            {
                Top = 250,
                Left = 15,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Top,
                Alignment = HorizontalAlignment.Center,
                Border = new BorderInfo(),
                ColumnWidths = "90 90 90 90 90 90",
                DefaultCellBorder = new BorderInfo(new GraphInfo())
            });
            _pdfApi.PostPageTables(fileName, 1, tables);
        }

        private void CreateInvoiceSomeSentence(string fileName, Invoice invoice)
        {
            var segments = new List<Segment>();
            segments.Add(new Segment(invoice.SomeSentence, new TextState(11, "Helvetica")));
            var lines = new List<TextLine>();
            lines.Add(new TextLine(Segments: segments));
            var paragraph = new Paragraph(Lines: lines);
            paragraph.VerticalAlignment = VerticalAlignment.Top;
            paragraph.HorizontalAlignment = TextHorizontalAlignment.None;
            paragraph.LeftMargin = 15;
            paragraph.RightMargin = 15;
            paragraph.TopMargin = 150;
            paragraph.BottomMargin = 150;
            _pdfApi.PutAddText(fileName, 1, paragraph);
        }

        private void CreateInvoiceInfo(string fileName, Invoice invoice)
        {
            var paragraphs11 = new List<TextRect>();
            paragraphs11.Add(new TextRect("Invoice ID", 1, HorizontalAlignment: HorizontalAlignment.Left));
            var cells1 = new List<Cell>();
            cells1.Add(new Cell()
            {
                Paragraphs = paragraphs11
            });

            var paragraphs12 = new List<TextRect>();
            paragraphs12.Add(new TextRect(": " + invoice.InvoiceID, 1, HorizontalAlignment: HorizontalAlignment.Left));
            cells1.Add(new Cell()
            {
                Paragraphs = paragraphs12
            });

            var paragraphs13 = new List<TextRect>();
            paragraphs13.Add(new TextRect(invoice.CustomerName, 1, HorizontalAlignment: HorizontalAlignment.Left));
            cells1.Add(new Cell()
            {
                Paragraphs = paragraphs13
            });

            var rows = new List<Row>();
            rows.Add(new Row(Cells: cells1)
            {
                DefaultCellTextState = new TextState(11, "Helvetica")
            });

            var paragraphs21 = new List<TextRect>();
            paragraphs21.Add(new TextRect("Clientnumber", 1, HorizontalAlignment: HorizontalAlignment.Left));
            var cells2 = new List<Cell>();
            cells2.Add(new Cell()
            {
                Paragraphs = paragraphs21
            });

            var paragraphs22 = new List<TextRect>();
            paragraphs22.Add(new TextRect(": " + invoice.CustomerNumber, 1, HorizontalAlignment: HorizontalAlignment.Left));
            cells2.Add(new Cell()
            {
                Paragraphs = paragraphs22
            });

            var paragraphs23 = new List<TextRect>();
            paragraphs23.Add(new TextRect(invoice.CustomerStreet, 1, HorizontalAlignment: HorizontalAlignment.Left));
            cells2.Add(new Cell()
            {
                Paragraphs = paragraphs23
            });

            rows.Add(new Row(Cells: cells2)
            {
                DefaultCellTextState = new TextState(11, "Helvetica")
            });

            var paragraphs31 = new List<TextRect>();
            paragraphs31.Add(new TextRect("VAT Number", 1, HorizontalAlignment: HorizontalAlignment.Left));
            var cells3 = new List<Cell>();
            cells3.Add(new Cell()
            {
                Paragraphs = paragraphs31
            });

            var paragraphs32 = new List<TextRect>();
            paragraphs32.Add(new TextRect(": " + invoice.CustomerVATNumber, 1, HorizontalAlignment: HorizontalAlignment.Left));
            cells3.Add(new Cell()
            {
                Paragraphs = paragraphs32
            });

            var paragraphs33 = new List<TextRect>();
            paragraphs33.Add(new TextRect(invoice.CustomerCity, 1, HorizontalAlignment: HorizontalAlignment.Left));
            cells3.Add(new Cell()
            {
                Paragraphs = paragraphs33
            });

            rows.Add(new Row(Cells: cells3)
            {
                DefaultCellTextState = new TextState(11, "Helvetica")
            });

            var tables = new List<Table>();
            tables.Add(new Table(Rows: rows)
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                Alignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Top,
                //Border = new BorderInfo(),
                ColumnWidths = "110 150 200",
                Left = 15,
                Top = 150
            });
            _pdfApi.PostPageTables(fileName, 1, tables);
        }

        private void CreateInvoiceTitle(string fileName, Invoice invoice)
        {
            var segments = new List<Segment>();
            segments.Add(new Segment("Invoice", new TextState(20, "Helvetica", FontStyle: FontStyles.Bold)));
            var lines = new List<TextLine>();
            lines.Add(new TextLine(Segments: segments));
            var paragraph = new Paragraph(Lines: lines);
            paragraph.LeftMargin = 15;
            paragraph.TopMargin = 50;
            paragraph.VerticalAlignment = VerticalAlignment.Top;
            _pdfApi.PutAddText(fileName, 1, paragraph);
        }

        private void CreateCompanyinfo(string fileName, Invoice invoice)
        {
            var paragraphs = new List<TextRect>();
            foreach (var item in invoice.HeaderCompanyInfo)
            {
                paragraphs.Add(new TextRect(item, 1, HorizontalAlignment: HorizontalAlignment.Left));
            }
            var cells = new List<Cell>();
            cells.Add(new Cell()
            {
                Paragraphs = paragraphs
            });
            var rows = new List<Row>();
            rows.Add(new Row(Cells: cells)
            {
                DefaultCellTextState = new TextState(8, "Helvetica"),
            });
            var tables = new List<Table>();
            tables.Add(new Table(Rows: rows)
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Top,
                Top = 10,
                Left = 15,
                Alignment = HorizontalAlignment.Center,
                Border = new BorderInfo(),
                ColumnWidths = "213"
            });
            _pdfApi.PostPageTables(fileName, 1, tables);
        }

        public void SaveAsImage(string srcFile, string outFileName)
        {
            int width = 800;
            int height = 1200;
            int leftMargin = 15;
            int rightMargin = 15;
            int topMargin = 15;
            int bottomMargin = 15;

            var apiResponse = _pdfApi.PutPageConvertToJpeg(srcFile, 1, outFileName, width, height);
            if (!(apiResponse != null && apiResponse.Status.Equals("OK")))
            {
                throw new HttpRequestException();
            }
            _pdfApi.DeleteFile(srcFile);
        }

    }
}
