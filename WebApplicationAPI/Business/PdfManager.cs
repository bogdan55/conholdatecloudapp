﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Aspose.Html.Cloud.Sdk.Api;
using Aspose.Html.Cloud.Sdk.Api.Interfaces;
using Aspose.Pdf.Cloud.Sdk.Api;
using Aspose.Pdf.Cloud.Sdk.Model;
using Microsoft.AspNetCore.Razor.Language;

namespace WebApplicationAPI.Business
{
    public class PdfManager
    {
        private readonly string _outputPath;
        private readonly HtmlApi _htmlApi;
        private readonly PdfApi _pdfApi;

        public PdfManager(string outputPath, HtmlApi htmlApi, PdfApi pdfApi)
        {
            _outputPath = outputPath;
            _htmlApi = htmlApi;
            _pdfApi = pdfApi;
        }

        public void FetchPages(string url, string fileName)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            var response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var str = response.Content.ReadAsStreamAsync().Result;
                _pdfApi.UploadFile(fileName, str);
            }
            else
            {
                throw new HttpRequestException();
            }
        }

        public void SaveAsPdf(string srcStream, string fileName)
        {
            int width = 800;
            int height = 1200;
            int leftMargin = 15;
            int rightMargin = 15;
            int topMargin = 15;
            int bottomMargin = 15;
            try
            {
                var apiResponse = _pdfApi.PutHtmlInStorageToPdf(fileName, srcStream,  null, height, width);

                if (!(apiResponse != null && apiResponse.Status.Equals("OK")))
                {
                    throw new HttpRequestException();
                }
            }
            catch (Exception ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public void SaveAsImage(string srcFile, string outFileName)
        {
            int width = 800;
            int height = 1200;
            int leftMargin = 15;
            int rightMargin = 15;
            int topMargin = 15;
            int bottomMargin = 15;

            var apiResponse = _htmlApi.PutConvertDocumentToImage(srcFile, "jpeg", outFileName, width, height);
            if (!(apiResponse != null && apiResponse.Status.Equals("OK")))
            {
                throw new HttpRequestException();
            }
        }

        public void MergeImagesToDoc(string fileName, int i, string ext)
        {
            var imgtempl = new ImageTemplatesRequest(false, ImagesList: new List<ImageTemplate>());
            for (int j = 1; j <= i; j++)
            {
                imgtempl.ImagesList.Add(new ImageTemplate($"{fileName}{j}.{ext}"));
            }
            var res = _pdfApi.PutImageInStorageToPdf(_outputPath, imgtempl);
            if (!(res.Code.HasValue && res.Code.Value == 200))
            {
                throw new HttpRequestException();
            }
            for (int j = 1; j <= i; j++)
            {
                _pdfApi.DeleteFile($"{fileName}{j}.pdf");
                _pdfApi.DeleteFile($"{fileName}{j}.{ext}");
            }
        }

        public void MergeDocs(string fileName, int i)
        {
            var mergeDoc = new MergeDocuments(new List<string>());
            for (int j = 1; j <= i; j++)
            {
                mergeDoc.List.Add($"{fileName}{j}.pdf");
            }
            var res = _pdfApi.PutMergeDocuments(_outputPath, mergeDoc);
            if (!(res.Code.HasValue && res.Code.Value == 200))
            {
                throw new HttpRequestException();
            }
            for (int j = 1; j <= i; j++)
            {
                _pdfApi.DeleteFile($"{fileName}{j}.pdf");
                _pdfApi.DeleteFile($"{fileName}{j}.html");
            }
        }

        public int GetPageCount()
        {
            return _pdfApi.GetPages(_outputPath).Pages.List.Count;
        }

        public void ExportPageAsImage(int i, string typeFile, string fileName)
        {
            var fullFileName = $"{fileName}{i}.{typeFile}";
            switch (typeFile)
            {
                case "jpg":
                {
                    _pdfApi.PutPageConvertToJpeg(_outputPath, i, fullFileName);
                    break;
                }
                case "png":
                {
                    _pdfApi.PutPageConvertToPng(_outputPath, i, fullFileName);
                    break;
                }
                default:
                {
                    throw new Exception("Unknown format");
                }
            }
        }
    }
}
