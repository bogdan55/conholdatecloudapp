﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Aspose.Pdf.Cloud.Sdk.Api;
using Microsoft.AspNetCore.Mvc;
using ConholdateCloudApp.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace ConholdateCloudApp.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(IHostingEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            //_hostingEnvironment = hostingEnvironment;
            //_httpContextAccessor = httpContextAccessor;
            string token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE1NzEzODgyMjAsImV4cCI6MTU3MTQ3NDYyMCwiaXNzIjoiaHR0cHM6Ly9hcGktcWEuYXNwb3NlLmNsb3VkIiwiYXVkIjpbImh0dHBzOi8vYXBpLXFhLmFzcG9zZS5jbG91ZC9yZXNvdXJjZXMiLCJhcGkucGxhdGZvcm0iLCJhcGkucHJvZHVjdHMiXSwiY2xpZW50X2lkIjoiY2NiYTYyMGEtMWY0Mi00MTRiLWJhN2MtYzFkMTgyNjBjMzYxIiwiY2xpZW50X2lkU3J2SWQiOiIiLCJzY29wZSI6WyJhcGkucGxhdGZvcm0iLCJhcGkucHJvZHVjdHMiXX0.iTyl_ojHd6KVHF2Psu27M7sAPDCIrSF-GBw8fi1oKRizNMi7kb8We02Tfp-TKINlZw_D_QnMUqMpF8MwXK7b6QcnxyZUhXMFAEAbZQeUe-kz2Iymdx-vdXKAOUx1Tw83po6MYJ-vlacDtnW0BneUFtKcp12klTDRKqN396xD85-mSotaiJjRjWv4TwUPrS52PH02OMN7eab4HDwOiSPmTgcxk0O7zwy5abXsuGrtkDKH1DzowQ546vxVcpF0IYjiJNlhZ3vAGdNPw5K6KGEn6INhHoZUrlANfGMEdN26-_7w5L02-amQLArkQYBwRinq55EEgnGEswmRHBhSPmOp9A";
            string bearerToken = string.Empty;
            Regex regexp = new Regex(@"Bearer\s+(?<token>\S+)", RegexOptions.IgnoreCase);
            Match match = regexp.Match(token);
            if (match.Success)
                bearerToken = match.Groups["token"].Value;
            var _pdfApi = new PdfApi(bearerToken);
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
